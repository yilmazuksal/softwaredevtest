﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public enum AccountTypes
    {        
        CHECKING = 0,        
        SAVINGS = 1,
        MAXI_SAVINGS = 2
    }
}
