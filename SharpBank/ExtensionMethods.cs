﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public static class ExtensionMethods
    {
        public static string ConvertToDollarString(this double amount)
        {
            return String.Format("${0:N2}", Math.Abs(amount));
        }

        public static string GetFriendlyName(this AccountTypes accountType)
        {
            switch (accountType)
            {
                case AccountTypes.CHECKING:
                    return "Checking Account";
                case AccountTypes.SAVINGS:
                    return "Savings Account";
                case AccountTypes.MAXI_SAVINGS:
                    return "Maxi Savings Account";
                default:
                    return null;
            }
        }

        public static string GetFriendlyName(this TransactionTypes transactionType)
        {
            switch (transactionType)
            {
                case TransactionTypes.DEPOSIT:
                    return "deposit";
                case TransactionTypes.WITHDRAWAL:
                    return "withdrawal";
                default:
                    return null;
            }
        }

    }
}
