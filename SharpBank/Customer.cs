﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharpBank
{
    public class Customer
    {
        private String name;
        private List<Account> accounts;

        public Customer(String name)
        {
            this.name = name;
            this.accounts = new List<Account>();
        }

        public String GetName()
        {
            return name;
        }

        public void OpenAccount(Account account)
        {
            accounts.Add(account);
        }

        public int GetNumberOfAccounts()
        {
            return accounts.Count;
        }

        public double TotalInterestEarned()
        {
            return accounts.Sum(a => a.InterestEarned());
        }

        public void Transfer(Account fromAccount,Account toAccount,double amount)
        {
            fromAccount.Transfer(amount, toAccount);
        }

        /*******************************
         * This method gets a statement
         *********************************/
        public String GetStatement()
        {
            StringBuilder statement = new StringBuilder(string.Empty);
            statement.AppendLine(string.Format("Statement for {0}", name));
            double total = 0.0;
            foreach (Account a in accounts)
            {
                statement.AppendLine();
                statement.Append(a.GetStatement());
                statement.AppendLine();
                total += a.SumTransactions();
            }
            statement.AppendLine();
            statement.Append(string.Format("Total In All Accounts {0}", total.ConvertToDollarString()));
            return statement.ToString();
        }


       
    }
}
