﻿using SharpBank.InterestCalculators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class Account
    {
        private System.Object transactionLock = new System.Object();
        private double currentBalance = 0; // Instead of calculating from transactions each time, keep currentBalance for performance
        private IInterestCalculator interestCalculator;
        private readonly AccountTypes accountType;
        public List<Transaction> transactions;

        public Account(AccountTypes accountType,IInterestCalculator interestCalculator)
        {
            this.accountType = accountType;
            this.transactions = new List<Transaction>();
            this.interestCalculator = interestCalculator;
        }

        public void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                lock (transactionLock)
                {
                    transactions.Add(new Transaction(TransactionTypes.DEPOSIT,amount));
                    currentBalance += amount;
                }
            }
        }

        public void Withdraw(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else if(amount > currentBalance)
            {
                throw new ArgumentException("amount must be less than current available balance");
            }
            else
            {
                lock (transactionLock)
                {
                    transactions.Add(new Transaction(TransactionTypes.WITHDRAWAL,amount));
                    currentBalance -= amount;
                }
            }
        }

        public double InterestEarned()
        {
            return interestCalculator.GetInterestAmount(currentBalance,this.transactions); 
        }

        public double SumTransactions()
        {
            return currentBalance;
        }

        public AccountTypes GetAccountType()
        {
            return accountType;
        }

        public String GetStatement()
        {
            StringBuilder statement = new StringBuilder(string.Empty);

            statement.AppendLine(this.accountType.GetFriendlyName());

            //Now total up all the transactions
            double total = SumTransactions();
            foreach (Transaction t in this.transactions)
            {
                statement.AppendLine(string.Format("  {0} {1}", t.GetTransactionType().GetFriendlyName(), t.amount.ConvertToDollarString()));
            }
            statement.Append(string.Format("Total {0}" , total.ConvertToDollarString()));
            return statement.ToString();
        }

        public void Transfer(double amount,Account toAccount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else if (amount > currentBalance)
            {
                throw new ArgumentException("amount must be less than current available balance for transfer");
            }
            else
            {
                Withdraw(amount);
                toAccount.Deposit(amount);
            }
        }

    }
}
