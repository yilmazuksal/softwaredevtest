﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public enum TransactionTypes
    {
        [Description("deposit")]
        DEPOSIT = 1,
        [Description("withdrawal")]
        WITHDRAWAL = 2
    }
}
