﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        public readonly double amount;

        private DateTime transactionDate;
        private TransactionTypes type;

        public Transaction(TransactionTypes type, double amount)
        {
            this.type = type;
            this.amount = amount;
            this.transactionDate = DateProvider.GetInstance().Now();
        }

        public TransactionTypes GetTransactionType()
        {
            return this.type;
        }

        public DateTime GetTransactionDate()
        {
            return this.transactionDate;
        }

    }
}
