﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpBank.InterestCalculators;

namespace SharpBank.Factories
{
    public static class AccountFactory
    {
        public static Account Create(AccountTypes type)
        {
            switch (type)
            {
                case AccountTypes.CHECKING:
                   return new Account(type, new CheckingAccountInterestCalculator());
                case AccountTypes.SAVINGS:
                   return new Account(type, new SavingsAccountInterestCalculator());
                case AccountTypes.MAXI_SAVINGS:
                    return new Account(type, new MaxiSavingsAccountInterestCalculator());
                default:
                    return null;
            }
        }
    }
}
