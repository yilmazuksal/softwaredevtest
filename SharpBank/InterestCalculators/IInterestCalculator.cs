﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.InterestCalculators
{
    public interface IInterestCalculator
    {
        double GetInterestAmount(double currentBalance,List<Transaction> transactions);
    }
}
