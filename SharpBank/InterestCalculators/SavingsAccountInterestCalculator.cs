﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.InterestCalculators
{
    class SavingsAccountInterestCalculator : IInterestCalculator
    {
        public double GetInterestAmount(double currentBalance, List<Transaction> transactions)
        {
            if (currentBalance <= 1000)
                return currentBalance * 0.001;
            else
                return 1 + ((currentBalance - 1000) * 0.002);
        }
    }
}
