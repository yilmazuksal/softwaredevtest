﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.InterestCalculators
{
    class MaxiSavingsAccountInterestCalculator : IInterestCalculator
    {
        public double GetInterestAmount(double currentBalance, List<Transaction> transactions)
        {
            bool anyWithdrawalLast10days = transactions
                .Count(t => t.GetTransactionType() == TransactionTypes.WITHDRAWAL
                && ((TimeSpan)(DateTime.Today - t.GetTransactionDate())).Days <= 10) == 0 ?
                false : true;


            if (anyWithdrawalLast10days)
                return currentBalance * 0.001;
            else
            {
                return currentBalance * 0.05;
            }
        }
    }
}
