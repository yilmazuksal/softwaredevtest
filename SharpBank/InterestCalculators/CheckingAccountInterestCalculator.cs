﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.InterestCalculators
{
    class CheckingAccountInterestCalculator : IInterestCalculator
    {
        public double GetInterestAmount(double currentBalance, List<Transaction> transactions)
        {
            return currentBalance * 0.001;
        }
    }
}
