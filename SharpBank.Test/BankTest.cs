﻿using NUnit.Framework;
using SharpBank.Factories;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        private static readonly double DOUBLE_DELTA = 1e-15;

        [Test]
        public void CustomerSummary()
        {
            Bank bank = new Bank();
            Customer john = new Customer("John");
            john.OpenAccount(AccountFactory.Create(AccountTypes.CHECKING));
            bank.AddCustomer(john);

            Assert.AreEqual("Customer Summary\n - John (1 account)", bank.CustomerSummary());
        }

        [Test]
        public void CheckingAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = AccountFactory.Create(AccountTypes.CHECKING);
            Customer bill = new Customer("Bill");
            bill.OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(100.0);

            Assert.AreEqual(0.1, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void SavingsAccount()
        {
            Bank bank = new Bank();
            Account savingsAccount = AccountFactory.Create(AccountTypes.SAVINGS);
            Customer bill = new Customer("Bill");
            bill.OpenAccount(savingsAccount);
            bank.AddCustomer(bill);

            savingsAccount.Deposit(1500.0);

            Assert.AreEqual(2.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccountWithNoWithdrawalInLastTenDays()
        {
            Bank bank = new Bank();
            Account maxiSavingsAccount = AccountFactory.Create(AccountTypes.MAXI_SAVINGS);
            Customer bill = new Customer("Bill");
            bill.OpenAccount(maxiSavingsAccount);
            bank.AddCustomer(bill);

            maxiSavingsAccount.Deposit(3000.0);

            Assert.AreEqual(150.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccountWithTransactionInLastTenDays()
        {
            Bank bank = new Bank();
            Account maxiSavingsAccount = AccountFactory.Create(AccountTypes.MAXI_SAVINGS);
            Customer bill = new Customer("Bill");
            bill.OpenAccount(maxiSavingsAccount);
            bank.AddCustomer(bill);

            maxiSavingsAccount.Deposit(3000.0);
            maxiSavingsAccount.Withdraw(1000.0);

            Assert.AreEqual(2.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }


    }
}
