﻿using NUnit.Framework;
using SharpBank.Factories;
using System;
using System.Threading;

namespace SharpBank.Test
{
    [TestFixture]
    public class AccountTest
    {
        [Test]
        public void TestDeposit()
        {
            Account account = AccountFactory.Create(AccountTypes.SAVINGS);

            account.Deposit(3000.0);
            Assert.AreEqual(3000.0, account.transactions[0].amount);
        }

        [Test]
        public void TestDepositWithNegativeAmount()
        {
            Account account = AccountFactory.Create(AccountTypes.SAVINGS);

            try
            {
                account.Deposit(-3000.0);
                Assert.Fail("An exception should have been thrown");
            }
            catch (ArgumentException ae)
            {
                Assert.AreEqual("amount must be greater than zero", ae.Message);
            }

        }

        [Test]
        public void TestDepositWithZeroAmount()
        {
            Account account = AccountFactory.Create(AccountTypes.SAVINGS);

            try
            {
                account.Deposit(0.0);
                Assert.Fail("An exception should have been thrown");
            }
            catch (ArgumentException ae)
            {
                Assert.AreEqual("amount must be greater than zero", ae.Message);
            }

        }



        [Test]
        public void TestWithdraw()
        {
            Account account = AccountFactory.Create(AccountTypes.SAVINGS);

            account.Deposit(3000.0);
            account.Withdraw(1000.0);
            Assert.AreEqual(2000.0, account.SumTransactions());
        }


        [Test]
        public void TestWithdrawWithAmountMoreThanCurrentBalance()
        {
            Account account = AccountFactory.Create(AccountTypes.SAVINGS);

            try
            {
                account.Withdraw(3000.0);
                Assert.Fail("An exception should have been thrown");
            }
            catch (ArgumentException ae)
            {
                Assert.AreEqual("amount must be less than current available balance", ae.Message);
            }

        }

        [Test]
        public void TestWithdrawWithNegativeAmount()
        {
            Account account = AccountFactory.Create(AccountTypes.SAVINGS);

            try
            {
                account.Withdraw(-3000.0);
                Assert.Fail("An exception should have been thrown");
            }
            catch (ArgumentException ae)
            {
                Assert.AreEqual("amount must be greater than zero", ae.Message);
            }

        }

        [Test]
        public void TestWithdrawWithZeroAmount()
        {
            Account account = AccountFactory.Create(AccountTypes.SAVINGS);

            try
            {
                account.Withdraw(0.0);
                Assert.Fail("An exception should have been thrown");
            }
            catch (ArgumentException ae)
            {
                Assert.AreEqual("amount must be greater than zero", ae.Message);
            }

        }


    }
}
