﻿using NUnit.Framework;
using SharpBank.Factories;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {

        [Test]
        public void TestCustomerStatementGeneration()
        {

            Account checkingAccount = AccountFactory.Create(AccountTypes.CHECKING);
            Account savingsAccount = AccountFactory.Create(AccountTypes.SAVINGS);

            Customer henry = new Customer("Henry");
            henry.OpenAccount(checkingAccount);
            henry.OpenAccount(savingsAccount);

            checkingAccount.Deposit(100.0);
            savingsAccount.Deposit(4000.0);
            savingsAccount.Withdraw(200.0);

            Assert.AreEqual("Statement for Henry\r\n" +
                    "\r\n" +
                    "Checking Account\r\n" +
                    "  deposit $100.00\r\n" +
                    "Total $100.00\r\n" +
                    "\r\n" +
                    "Savings Account\r\n" +
                    "  deposit $4,000.00\r\n" +
                    "  withdrawal $200.00\r\n" +
                    "Total $3,800.00\r\n" +
                    "\r\n" +
                    "Total In All Accounts $3,900.00", henry.GetStatement());
        }


        [Test]
        public void TestThreeAccounts()
        {
            Customer oscar = new Customer("Oscar");
            Account savingsAccount = AccountFactory.Create(AccountTypes.SAVINGS);
            Account checkingAccount = AccountFactory.Create(AccountTypes.CHECKING);
            Account maxiSavingsAccount = AccountFactory.Create(AccountTypes.MAXI_SAVINGS);

            oscar.OpenAccount(savingsAccount);
            oscar.OpenAccount(checkingAccount);
            oscar.OpenAccount(maxiSavingsAccount);

            Assert.AreEqual(3, oscar.GetNumberOfAccounts());
        }



        [Test]
        public void TestTransferMoneyBetweenAccounts()
        {
            Customer oscar = new Customer("Oscar");
            Account transferFrom = AccountFactory.Create(AccountTypes.SAVINGS);
            Account transferTo = AccountFactory.Create(AccountTypes.SAVINGS);
            oscar.OpenAccount(transferFrom);
            oscar.OpenAccount(transferTo);

            transferFrom.Deposit(3000.0);
            transferTo.Deposit(1500.0);
            
            oscar.Transfer(transferFrom, transferTo, 750.0);      

            Assert.AreEqual(transferFrom.SumTransactions(),transferTo.SumTransactions());
        }

        [Test]        
        public void TestTransferMoneyBetweenAccountsWithAmountMoreThanCurrentBalance()
        {
            Customer oscar = new Customer("Oscar");
            Account transferFrom = AccountFactory.Create(AccountTypes.SAVINGS);
            Account transferTo = AccountFactory.Create(AccountTypes.SAVINGS);
            oscar.OpenAccount(transferFrom);
            oscar.OpenAccount(transferTo);

            transferFrom.Deposit(3000.0);
            transferTo.Deposit(1500.0);

            try
            {
                oscar.Transfer(transferFrom, transferTo, 4000.0);
                Assert.Fail("An exception should have been thrown");
            }                        
            catch(ArgumentException ae)
            {
                Assert.AreEqual("amount must be less than current available balance for transfer", ae.Message);
            }

        }

        [Test]
        public void TestTransferMoneyBetweenAccountsWithNegativeAmount()
        {
            Customer oscar = new Customer("Oscar");
            Account transferFrom = AccountFactory.Create(AccountTypes.SAVINGS);
            Account transferTo = AccountFactory.Create(AccountTypes.SAVINGS);
            oscar.OpenAccount(transferFrom);
            oscar.OpenAccount(transferTo);

            transferFrom.Deposit(3000.0);
            transferTo.Deposit(1500.0);

            try
            {
                oscar.Transfer(transferFrom, transferTo, -200.0);
                Assert.Fail("An exception should have been thrown");
            }
            catch (ArgumentException ae)
            {
                Assert.AreEqual("amount must be greater than zero", ae.Message);
            }

        }


        [Test]
        public void TestTransferMoneyBetweenAccountsWithZeroAmount()
        {
            Customer oscar = new Customer("Oscar");
            Account transferFrom = AccountFactory.Create(AccountTypes.SAVINGS);
            Account transferTo = AccountFactory.Create(AccountTypes.SAVINGS);
            oscar.OpenAccount(transferFrom);
            oscar.OpenAccount(transferTo);

            transferFrom.Deposit(3000.0);
            transferTo.Deposit(1500.0);

            try
            {
                oscar.Transfer(transferFrom, transferTo, 0.0);
                Assert.Fail("An exception should have been thrown");
            }
            catch (ArgumentException ae)
            {
                Assert.AreEqual("amount must be greater than zero", ae.Message);
            }

        }


    }
}
